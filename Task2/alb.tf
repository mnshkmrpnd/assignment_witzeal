provider "aws" {
  region = "us-west-2" # Replace with your desired region
}

resource "aws_lb" "example" {
  name               = "example-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.example.id] # Replace with your security group ID

  subnet_mapping {
    subnet_id = aws_subnet.example1.id # Replace with your subnet ID
  }

  subnet_mapping {
    subnet_id = aws_subnet.example2.id # Replace with your subnet ID
  }

  tags = {
    Name = "example-alb"
  }
}

resource "aws_lb_target_group" "example" {
  name_prefix = "example-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.example.id # Replace with your VPC ID

  health_check {
    interval            = 30
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 10
    healthy_threshold   = 2
    unhealthy_threshold = 5
  }

  tags = {
    Name = "example-tg"
  }
}

resource "aws_lb_listener" "example" {
  load_balancer_arn = aws_lb.example.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.example.arn
  }
}

resource "aws_autoscaling_group" "example" {
  name                 = "example-asg"
  desired_capacity     = 1
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = [aws_subnet.example1.id, aws_subnet.example2.id] # Replace with your subnet IDs
  launch_configuration = aws_launch_configuration.example.id # Replace with your launch configuration ID

  tag {
    key                 = "Name"
    value               = "example-instance"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "dev"
    propagate_at_launch = true
  }
}

resource "aws_launch_configuration" "example" {
  name_prefix          = "example-lc"
  image_id             = "ami-0c55b159cbfafe1f0" # Replace with your desired AMI ID
  instance_type        = "t2.micro"
  security_groups      = [aws_security_group.example.id] # Replace with your security group ID
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello, World!" > /var/www/html/index.html
              EOF
}

resource "aws_security_group" "example" {
  name_prefix = "example-sg"
  vpc_id      = aws_vpc.example.id # Replace with your VPC ID

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "example-s

