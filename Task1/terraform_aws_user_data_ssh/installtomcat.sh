#! /bin/bash
sudo yum -y update
#Install Redis
sudo yum install epel-release -y
sudo yum update -y
sudo yum install redis -y
sudo systemctl start redis
sudo systemctl enable redis
# Install Java              
sudo amazon-linux-extras install java-openjdk11 -y
sudo su -
# Clone war file
cd /opt
git clone https://github.com/git-tirumalarao/sample-war-files.git
#Download tomcat binary
wget https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.55/bin/apache-tomcat-9.0.55.tar.gz
#unzip tomcat binary
tar -zvxf apache-tomcat-9.0.55.tar.gz
cd apache-tomcat-9.0.55
cd bin
chmod +x startup.sh
chmod +x shutdown.sh
ln -s /opt/apache-tomcat-9.0.55/bin/startup.sh /usr/local/bin/tomcatup
ln -s /opt/apache-tomcat-9.0.55/bin/shutdown.sh /usr/local/bin/tomcatdown
sudo cp /opt/CounterWebApp.war /usr/share/tomcat/webapps/
sudo systemctl enable tomcat
sudo systemctl restart tomcat
sudo systemctl status tomcat


